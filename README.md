# GitLab Submission System

This project provides a set of scripts and a dev container to run them
in for using GitLab as a submission system for a course.

## Documentation

* [Instructor](docs/Instructor/README.md)
* [Student](docs/Student/README.md)
* [Developer](docs/Developer/README.md)
