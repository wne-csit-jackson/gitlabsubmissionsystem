#!/usr/bin/env bash

set -euo pipefail

export PREP_REPO=prep
export INDIVIDUAL_DIR=individual
export INIT_PROJECT=
export GITLAB_GROUP=

main() {
    process_command_line "$@"
    init_student_repos
}

process_command_line() {
    if [ $# -lt 1 ] || [ $# -gt 1 ]
    then
        print_usage
        if [ $# -gt 2 ]
        then
            echo "ERROR: Extra arguments detected. Need quotes?"
        fi
        exit
    fi

    GITLAB_GROUP="${1}"

    if [ $# -eq 2 ]
    then
        INIT_PROJECT="${2}"
    fi
}

print_usage() {
    echo "
Reads names from stdin, creates local repos and repos
projects on GitLab for each student in the specified
group.

Run 'glab auth login' before running this command.

USAGE
    $0 <group>

ARGS
    <group>     GitLab group to hold student repos.
    stdin       Names of students, one per line.


EXAMPLES

    Create initial repositories from a list of names
    in a file named names.txt.

        $ cat names.txt
        s1
        s2
        $ glab auth login
        $ $0 wne-cs200-01-f2023 < names.txt
"
}

init_student_repos() {
    mkdir -p "${INDIVIDUAL_DIR}"
    set +e
    while read -r name
    do
        init_student "${name}"
    done
    set -e
}

init_student() (
    set -eu
    local name=
    name="${1}"

    if [ -d "${INDIVIDUAL_DIR}/${name}" ] ; then
        >&2 echo "${name} exists. Skipping."
        return
    fi

    git clone "${PREP_REPO}/.git" "${INDIVIDUAL_DIR}/${name}"
    cd "${INDIVIDUAL_DIR}/${name}"
    git remote rename origin upstream
    glab repo create \
        --private \
        --defaultBranch main \
        --group "${GITLAB_GROUP}"
    sleep 5
    git push -u origin main
)

main "$@"
