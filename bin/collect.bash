#!/usr/bin/env bash

set -euo pipefail

export PREP_REPO=prep
export INDIVIDUAL_DIR=individual

main() {
    set +e
    for i in individual/*
    do
    (
        set -e
        git -C "$i" restore .
        git -C "$i" clean -xfd
        git -C "$i" switch main
        git -C "$i" pull --rebase origin main
    )
    done
}

main "$@"
