#!/usr/bin/env bash

set -euo pipefail

export PREP_REPO=prep
export INDIVIDUAL_DIR=individual

main() {
    validate_command_line "$@"

    local message=
    message="$1"

    commit_work_in_prep "${message}"

    local commit=
    commit="$(get_last_commit_hash prep)"

    set +e      # continue processing even if a command fails
    for s in "${INDIVIDUAL_DIR}"/*
    do
        post_to_student "${s}" "${commit}"
    done
    set -e
}

validate_command_line() {
    if [ -z "${1+x}" ] ; then
        echo "The first argument must be the commit message."
        print_usage
        exit
    fi

    if [ $# -ne 1 ] ; then
        print_usage
        echo
        echo "ERROR: Too many arguments. Forget to quote your message?"
        exit
    fi
}

print_usage() {
    echo "
Posts changes in prep/ to each repo in individual/.

Note this throws away any changes you have made to individual/* projects.
If you have feedback to return in those projects, first run bin/feedback.bash .

USAGE
    $0 <message>

ARGS
    <message>   Commit message passed to git.

EXAMPLES

    Prepare and post hw1

        # Copy last years hw1, and modify it.
        $ cp ../lastyear/prep/hw1 prep/hw1
        $ vim prep/hw1/README.md

        # Post it.
        $ $0 "hw1: assigned"

TIPS

    1. To prevent git conflicts, avoid changing files that you
       have already posted. Especially files that you expect students
       to modify. Instead create a new, unique file, post it, and tell
       your students what you want them to do with that file.

    2. Do watch for conflicts or other errors in the output.
"
}

commit_work_in_prep() {
    git -C "${PREP_REPO}" stage .
    git -C "${PREP_REPO}" commit -m "$1" || true
}

get_last_commit_hash() {
    git -C "${1}" rev-parse HEAD
}

post_to_student() (
    set -eu
    local repo=
    local commit=
    repo="${1}"
    commit="${2}"

    local last_commit_message=
    last_commit_message="$(git -C "${repo}" log -1)"
    if [[ "${last_commit_message}" =~ "${commit}" ]]
    then
        echo "Skipping ${repo}. It has commit ${commit}."
    else
        git -C "${repo}" restore .
        git -C "${repo}" clean -xfd
        git -C "${repo}" switch main
        git -C "${repo}" fetch upstream
        git -C "${repo}" cherry-pick -x "${commit}"
        git -C "${repo}" pull --rebase origin main
        git -C "${repo}" push origin main
    fi
)

main "$@"
