#!/usr/bin/env bash

set -euo pipefail

export PREP_REPO=prep
export INDIVIDUAL_DIR=individual

main() {
    validate_command_line "$@"

    local commit=
    commit="$1"

    set +e      # continue processing even if a command fails
    for s in "${INDIVIDUAL_DIR}"/*
    do
        post_to_student "${s}" "${commit}"
    done
    set -e
}

validate_command_line() {
    if [ -z "${1+x}" ] ; then
        echo "The first argument must be the commit message."
        print_usage
        exit
    fi

    if [ $# -ne 1 ] ; then
        print_usage
        echo
        echo "ERROR: Too many arguments. Forget to quote your message?"
        exit
    fi
}

print_usage() {
    echo "
Posts a specific commit in prep to individuals. This is useful if you
made some commits in prep that you did not post and now want to.

Note this throws away any changes you have made to individual/* projects.
If you have feedback to return in those projects, first run bin/feedback.bash .

USAGE
    $0 <commit>

ARGS
    <commit>   Commit hash in prep to post to individuals.

EXAMPLES

    Posting a commit.

        # Use git log to identify the hash of the commit to post.
        $ cd prep
        $ git log
        c51b8679a6e04c0665f73a06dbe81373a31c0da3 ...
        cd ..

        # Post it
        $ $0 c51b8679a6e04c0665f73a06dbe81373a31c0da3

TIPS

    1. To prevent git conflicts, avoid changing files that you
       have already posted. Especially files that you expect students
       to modify. Instead create a new, unique file, post it, and tell
       your students what you want them to do with that file.

    2. Do watch for conflicts or other errors in the output.
"
}

post_to_student() (
    set -eu
    local repo=
    local commit=
    repo="${1}"
    commit="${2}"

    git -C "${repo}" restore .
    git -C "${repo}" clean -xfd
    git -C "${repo}" switch main
    git -C "${repo}" fetch upstream
    git -C "${repo}" cherry-pick -x "${commit}"
    git -C "${repo}" pull --rebase origin main
    git -C "${repo}" push origin main
)

main "$@"
