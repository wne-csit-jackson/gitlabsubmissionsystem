#!/usr/bin/env bash

set -euo pipefail

export PREP_REPO=prep
export INDIVIDUAL_DIR=individual

main() {
    set +e
    for i in individual/*
    do
    (
        set -e
        git -C "$i" stage .
        git -C "$i" commit -m "feedback"
        git -C "$i" push origin main
    )
    done
}

main "$@"
