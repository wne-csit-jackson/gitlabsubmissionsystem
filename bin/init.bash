#!/usr/bin/env bash

set -euo pipefail

export PREP_REPO=prep
export INDIVIDUAL_DIR=individual
export INIT_PROJECT=
export GITLAB_GROUP=

main() {
    process_command_line "$@"
    # authenticate
    init_prep_repo
    init_student_repos
}

process_command_line() {
    if [ $# -lt 1 ] || [ $# -gt 2 ]
    then
        print_usage
        if [ $# -gt 2 ]
        then
            echo "ERROR: Extra arguments detected. Need quotes?"
        fi
        exit
    fi

    GITLAB_GROUP="${1}"

    if [ $# -eq 2 ]
    then
        INIT_PROJECT="${2}"
    fi
}

print_usage() {
    echo "
Reads names from stdin, creates local repos and repos
projects on GitLab for each student in the specified
group.

Run 'glab auth login' before running this command.

USAGE
    $0 <group> [<project>]

ARGS
    <group>     GitLab group to hold student repos.
    <project>   Optional. A URL to a project to use as the initial
                project. By default, an empty project is created.
    stdin       Names of students, one per line.


EXAMPLES

    Create initial repositories from a list of names
    in a file named names.txt.

        $ cat names.txt
        s1
        s2
        $ glab auth login
        $ $0 wne-cs200-01-f2023 < names.txt
"
}

authenticate() {
    echo "Running 'glab auth login'. Follow prompts to authenticate with GitLab."
    glab auth login
}

init_prep_repo() {
    if [ -n "${INIT_PROJECT}" ]
    then
        git clone "${INIT_PROJECT}" "${PREP_REPO}"
    else
        git init "${PREP_REPO}"
        touch "${PREP_REPO}/README.md"
        git -C "${PREP_REPO}" stage .
        git -C "${PREP_REPO}" commit -m "init"
    fi
}

init_student_repos() {
    mkdir "${INDIVIDUAL_DIR}"
    set +e
    while read -r name
    do
        init_student "${name}"
    done
    set -e
}

init_student() (
    set -eu
    local name=
    name="${1}"

    git clone "${PREP_REPO}/.git" "${INDIVIDUAL_DIR}/${name}"
    cd "${INDIVIDUAL_DIR}/${name}"
    git remote rename origin upstream
    glab repo create \
        --private \
        --defaultBranch main \
        --group "${GITLAB_GROUP}"
    sleep 5
    git push -u origin main
)

main "$@"
