# Developer

Below is a detailed walk through of what an instructor has to do manually.
From it we extract what can be automated. This document can also be used
to understand in more detail what different scripts are doing.

This document will not be kept in synch with the implementation. Over time
it will become less and less accurate. Code is king.

## Prepare to use this project for a course

To use this project,

1. Make sure Docker is running.

2. Clone this project for your course. For example, if our course is cs220,
     in a terminal

    ```bash
    git clone <clone-url-for-this-project> cs220
    ```

3. Open the project's root direcotry in VS Code. Continuing from the previous
    example

    ```bash
    code cs220
    ```

4. When prompted to `Reopen in container`, do so. If you miss the prompt,
    click the `><` icon in the lower left and select `Reopen in container`
    from the menu.

5. Once running inside the devcontainer, open a terminal inside the
    devcontainer by pressing `CTRL` + `SHIFT` + `.

6. From within the root of the project call scripts in `bin/`.
    For example, `bin/init.bash`.

## Overview

This is a high-level view of the structure that this project helps to create
and uses to post content and collect work.

```plantuml
@startuml
actor Instructor
database prep
database "s1" as s1_i
database "s2" as s2_i
database "s1" as s1_s
database "s2" as s2_s
actor S1
actor S2

cloud GitLab {
    package "wne-cs200-01-f2023" {
        database s1
        database s2
    }
}

Instructor -- s1_i
Instructor -- s2_i
Instructor -- prep
s1_i -- s1
s2_i -- s2
s1 -- s1_s
s2 -- s2_s
s1_s -- S1
s2_s -- S2
@enduml
```

## `bin/init.bash`

Locally we want to create the following structure.

```plantuml
@startuml
database prep
package individual {
    database s1
    database s2
}
prep <-- s1 : upstream
prep <-- s2 : upstream
@enduml
```

To do this, we start by creating an empty `prep` repository.

```bash
git init prep
```

Next we create an `individual` directory that will hold
each student's repository.

```bash
mkdir individual
```

Then, for each student, we create the student's repository
by cloning it from `prep`.

```bash
cd individual
git clone ../prep/.git s1
git clone ../prep/.git s2
...
cd ..   # we're now in individual
```

Notice that this means that `prep` is considered `origin` from
the perspective of each student repository. But we want this to
be `upstream`. So now we need to rename `origin` to `upstream`
in each student repo.

```bash
cd s1
git remote rename origin upstream
cd ../s2
git remote rename origin upstream
...
cd ..   # we're now in individual
```

We suggest naming each project the last name of each student.
If two students have the same last name, then add their first
initial to the end. Do not include spaces or other punctuation.

Next we create a public or private group on GitLab that will
contain each student project. This will be the only group for
our class on GitLab, so we'll call it `wne-cs200-01-f2023`
and follow GitLab's directions to
[create a group](https://docs.gitlab.com/ee/user/group/#create-a-group).
If you have a more elaborate plan for your course on GitLab,
then the new group is probably a subgroup of
`wne-cs200-01-f2023` and could be called `individual`
or `student`; and we would follow GitLab's directions to
[create a subgroup](https://docs.gitlab.com/ee/user/group/subgroups/index.html#create-a-subgroup).

Now that we have a GitLab group named `wne-cs200-01-f2023` to
hold student projects, we create each student's private
project on GitLab. Manually, on GitLab, we create each
project as an empty project (no README, no License, nothing).
GitLab gives us a set of commands that we copy and paste in
the corresponding student's directory.

More easily we can use glab. First, we authenticate. Then
for each student's repo we create the repo on GitLab
and push the local up to gitlab.

```bash
cd s1
glab repo create --defaultBranch main --private --group wne-cs200-01-f2023
git push
cd ../s2
glab repo create --defaultBranch main --private --group wne-cs200-01-f2023
git push
```

With this done, our initial setup is complete.

Almost all of this can be automated. Create the GitLab group that will
hold student projects then run `bin/init.bash`.

Now we can populate student repositories with the initial content for
our class.

## `bin/post.bash`

In this section we use our setup to post content to our students'
repositories. This procedure can be used to publish the initial content,
and repeated to publish each increment of material we want to release
to our students.

As an example, let's say we want to release the hw1 assignment.

1. The instructor adds hw1 to `prep` and commits it.

    ```bash
    cp -R source/hw1 prep
    cd prep
    git stage .
    git commit -m "hw1: assigned"
    git log     # copy the hash of "hw1: assigned"
    cd ..
    ```

    The first line is copying hw1 from some other source. This could be
    our previous years' hw1. Or we could start from scratch and create
    hw1 and its contents in prep directly. Either way, after line one
    hw1 is ready to be distributed to students.

2. For each student, the instructor

    2.1. Cleans up the student repo

    ```bash
    cd individual/s1
    git restore .
    git clean -xfd
    ```

    2.2. Fetches the changes from `prep` and `cherry-picks` the new commit into the student's repo.

    ```bash
    git fetch upstream
    git cherry-pick <paste-hash-from-previous-step>
    ```

    2.3. Pushes the new change to the student's repo on GitLab.

    ```bash
    git pull --rebase
    git push
    ```

    2.4. Exit student repo directory and prepare for next iteration.

    ```bash
    cd ../..
    ```

Again, everything but the first line can be automated. You can
use `bin/post.bash` to post files from prep to students.

## Grant access

So far students still don't have access to their individual projects on
GitLab. This allows us to populate their projects with an initial set
of files before granting them access.

Now let's grant them access.

Have students create a GitLab account.

Collect student's usernames. When logged in, they click on their avatar
in the upper right corner, and their user name is the name that begins
with @. Since usernames do not have to reflect the students' real names,
be sure to record them along with their real name.

With this list, on GitLab, add each student's username to their individual
project as a Maintainer.

Yes, this can be automated. However, it has not yet been. Sorry.

## `bin/collect.bash`

To collect work from students, we clean up each local repo and pull.

```bash
cd individual/s1
git restore .
git clean -xfd
git switch main
git pull --rebase origin main
cd ../individual/s2
...
```

This has been automated. Try `bin/collect.bash`
