# Student - Getting Started

## Prerequisites to install

* [Git](https://git-scm.com/)
* [Docker Desktop](https://www.docker.com/products/docker-desktop/) (requires 8GB RAM)
* [VS Code](https://code.visualstudio.com/)
* [Dev Container extensions for VS Code](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)

## Configure Git

Git writes a name and email to each commit made to identify the author
of each commit. Choose a name and an email that you are comfortable with
becoming public. The email must be real, and you must have access to it.
It may be a throw-away email that you check infrequently.

```bash
# In any terminal; replace YOUR NAME and YOUR EMAIL
git config --global user.name "YOUR NAME"
git config --global user.email "YOUR EMAIL"
git config --global core.editor "code --wait"
git config --global pull.ff only
```

Repeat this configuration on each machine on which you work.

In a public lab, you'll need to perform this configuration when you sit
down. When you leave you should clear your name and email by setting
them to something else. If the lab computer resets this information on
reboot, just reboot.

## Create a GitLab account

If you already have one, you don't need to create another. But you
should read this section anyway.

Your account name is public, you may choose any safe-for-work name.
The email you use is private, but GitLab sends it notifications and
other GitLab users can contact you indirectly through GitLab; you can
control notifications through GitLab's settings. This email must be real,
you have access to it, and you should check it regularly.

The email you use to sign up with GitLab may be different from the one
you gave git. If it is different, add the email you gave to Git to your
GitLab account (in settings) so that GitLab can give you credit for
commits you author.

If you signed up using a service like Google, set a password for your
GitLab account (in settings), and use this if Git ever asks you for your
GitLab account and password.

## Give GitLab account name to your instructor

When asked, give your instructor your GitLab account name. To find your account
name, log into GitLab, and click your avatar;
your account name is the name that starts with `@`.

## Clone your project

1. Navigate to your project.

    1.1. Your instructor will provide you a GitLab link. Navigate to this link.
    1.2. Assuming you are signed into GitLab, you should see a private project
    with your name on it. Click it.
    1.3. Click the `code` pill and copy the HTTPS URL.

2. Clone the repository

This only needs to be done once per machine you work on.

If you use a lab
computer that resets on reboot, you'll need to do this each time you sit down.
Reboot your computer at the end of your work session so others don't have
access to it.

```bash
# Open a terminal; replace <course> with the name of your course e.g., cs220
git clone <paste-url> <course>
```

This clones your project into a directory in your home directory.
For example, on Mac or Linux this is something like `/home/<username>/<course>`.
On Windows this is something like `\user\<username>\<course>`.
It's important to be able to reliably find this project. Use your systems
file browser to find this folder now. If you can't, ask your instructor for
help.

## Work in this local clone.

Make sure Docker is running. Then...

```bash
# Open a terminal; replace <course> with the same name you used above.
cd <course>
code .
```

If VS Code prompts you to `Reopen in container`, do so. If you miss
this prompt, click `><` in the lower left and select `Reopen in container`
from the menu.

## Synchronize

Regularly synchronize your local work with your GitLab project.
We suggest synchronizing at the start and end of each work session
(a work session is when you sit down to work).

1. Open a terminal in the VS Code viewing your project: `CTRL`+`SHIFT`+`.

2. Run

    ```bash
    # In terminal in the VS Code viewing your project
    git stage . && git commit --allow-empty -m "sync" && git pull --rebase && git push
    ```

    If there are any errors let your instructor know ASAP.

Note you can have a clone on each computer you work on. As long as you
sync them at the start and end of your work session, your work will be
saved and you'll be able to pick up where you left off wherever you go.

## Check your work

Confirm that your work has been properly submitted by navigating to your
personal project for this course on GitLab, and inspect the files that you
have modified.
