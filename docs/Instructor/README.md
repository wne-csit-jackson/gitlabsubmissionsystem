# Instructor - Getting Started

## Prerequisites to install

* [Git](https://git-scm.com/)
* [Docker Desktop](https://www.docker.com/products/docker-desktop/) (requires 8GB RAM)
* [VS Code](https://code.visualstudio.com/)
* [Dev Container extensions for VS Code](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)

## Configure Git

Git writes a name and email to each commit made to identify the author
of each commit. Choose a name and an email that you are comfortable with
becoming public. The email must be real, and you must have access to it.
It may be a throw-away email that you check infrequently.

```bash
# In any terminal
git config --global user.name "YOUR NAME"
git config --global user.email "YOUR EMAIL"
git config --global core.editor "code --wait"
git config --global pull.ff only
```

Repeat this configuration on each machine on which you work.

In a public lab, you'll need to perform this configuration when you sit
down. When you leave you should clear your name and email by setting
them to something else. If the lab computer resets this information on
reboot, just reboot.

## Create a GitLab account

If you already have one, you don't need to create another. But you
should read this section anyway.

Your account name is public, you may choose any safe-for-work name.
The email you use is private, but GitLab sends it notifications and
other GitLab users can contact you indirectly through GitLab; you can
control notifactions through GitLab's settings. This email must be real,
you have access to it, and you should check it regularly.

The email you use to sign up with GitLab may be different from the one
you gave git. If it is different, add the email you gave to Git to your
GitLab account (in settings) so that GitLab can give you credit for
commits you author.

If you signed up using a service like Google, set a password for your
GitLab account (in settings), and use this if Git ever asks you for your
GitLab account and password.

## Create a GitLab Group

On GitLab create a public group to hold private student projects.
The group does not have to be public, but it makes it easier for students
to find their private project.

Choose a name that encodes your school
and course offering information: e.g., e.g., wne-cs220-02-s2023.

The group can also be a subgroup. For example, maybe you have a group
named wne-cs220-02-s2023 and plan to have other projects and subgroups
in here to organize your course. Then within this group you may want to
create a subgroup named `individual` or `student` to hold private student
projects.

## Clone this project and open it in VS Code

```bash
# In any terminal
git clone <this-project> cs220-01
cd course
code .
```

## Create initial repositories

1. In the root of the project, use VS Code to create a list of students,
    one per line, in names.txt (no spaces or punctuation).

2. Create repositories prep/ and individual/* and student projects on GitLab.

    ```bash
    # In Dev Container terminal
    bin/init.bash wne-cs220-02-s2023 < names.txt
    ```

## Grant students access

On GitLab add each student's GitLab account as Maintainer to their project.
Give students a URL to the GitLab group, make sure they are signed in,
and they should see their project.

## Post content

Note, this will throw away any changes you have made to any individual/*
project. So if you have feedback you want to return, be sure to run
bin/feedback.bash first.

1. Add content to prep/

2. Post content to students

    ```bash
    # In Dev Container terminal
    bin/post.bash
    ```

## Collect student work

```bash
# In Dev Container terminal
bin/collect.bash
```

## Post individual feedback

If you want to leave feedback for students in their work...

1. Grade each individual/* project, leaving feedback as you go.
2. After all have been graded, run the following to push up all your
    changes.

    ```bash
    bin/feedback.bash
    ```
